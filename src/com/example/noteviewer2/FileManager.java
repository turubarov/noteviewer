package com.example.noteviewer2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.widget.Toast;

import com.example.noteviewer2.R;

/**
 * �����, ���������� �� ������ � �������
 * 
 * @author ���������
 * 
 */
public class FileManager {
	private static FileManager instance = new FileManager();

	private FileManager() {
		isInit = false;
	}

	public static FileManager getInstance() {
		return instance;
	}

	private Context applicationContext; // �������� ����������
	private String homeDirectoryPath; // �������� ���������� ����������
	private boolean isInit;

	public void init(Context context, String homeDirectoryArg) {
		if (!isInit)
		{
			applicationContext = context;
			homeDirectoryPath = homeDirectoryArg;
			isInit = true;
		}
	}

	public boolean saveToFile(String stringForSave, String fileName) {
		try {
			FileOutputStream outputstream = new FileOutputStream(homeDirectoryPath + "/" + fileName);
			OutputStreamWriter osw = new OutputStreamWriter(outputstream);
			osw.write(stringForSave);
			osw.close();
			return true;
		} catch (Throwable t) {
			Toast.makeText(applicationContext, "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
			return false;
		}
	}

	public String loadFromFile(String fileName) {
		try {
			FileInputStream inputstream = new FileInputStream(homeDirectoryPath + "/" + fileName);

			InputStreamReader isr = new InputStreamReader(inputstream);
			BufferedReader reader = new BufferedReader(isr);
			String str;
			StringBuffer buffer = new StringBuffer();

			while ((str = reader.readLine()) != null) {
				buffer.append(str + "\n");
			}

			inputstream.close();
			return buffer.toString();
		} catch (Throwable t) {
			Toast.makeText(applicationContext, "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
			return "";
		}
	}

	public String addStyles(String userString) {
		return "<html>" + "<head><style>" + applicationContext.getString(R.string.body_style)
				+ applicationContext.getString(R.string.h1_style) + applicationContext.getString(R.string.h2_style)
				+ applicationContext.getString(R.string.h3_style) + applicationContext.getString(R.string.h4_style)
				+ applicationContext.getString(R.string.h5_style) + applicationContext.getString(R.string.h6_style)
				+ applicationContext.getString(R.string.blockquote_style) + applicationContext.getString(R.string.pre_style)
				+ "</style></head>" + "<body>" + userString + "</body></html>";
	}
}
