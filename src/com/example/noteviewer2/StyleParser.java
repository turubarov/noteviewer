package com.example.noteviewer2;

import android.content.Context;

/**
 * ����� ��� ������ �� ������� ������������ html �������
 * 
 * @author ���������
 *
 */
public class StyleParser {
	private Context appContext;

	public StyleParser(Context context) {
		appContext = context;
	}

	/**
	 * ��������� ����� � html ��������
	 * 
	 * @param text	����� html ��������
	 */
	public String addStyle(String text) {
		return "<html>" + "<head><style>" + appContext.getString(R.string.body_style) + appContext.getString(R.string.h1_style)
				+ appContext.getString(R.string.h2_style) + appContext.getString(R.string.h3_style)
				+ appContext.getString(R.string.h4_style) + appContext.getString(R.string.h5_style)
				+ appContext.getString(R.string.h6_style) + appContext.getString(R.string.blockquote_style)
				+ appContext.getString(R.string.pre_style) + "</style></head>" + "<body>" + text + "</body></html>";
	}
}
