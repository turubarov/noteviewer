package com.example.noteviewer2;

import android.content.Context;

/**
 * �������, ����� ��� ����� ����������
 * 
 * @author ���������
 *
 */
public class Utils {
	private Context appContext;
	public Utils(Context context) {
		appContext = context;
	}
	/**
	 * ������ true, ���� url - ������ �� ���������� ��������. ����� - false
	 */
	public boolean isInnerPage(String url) {
		return !url.startsWith("http");
	}
	
	/**
	 * �������� url �������� �� � �����
	 * @param name
	 * @return
	 */
	public String getUrlFromName(String name) {
		return "file://" + appContext.getFilesDir().getAbsolutePath() + "/" + name + ".html";
	}
}
