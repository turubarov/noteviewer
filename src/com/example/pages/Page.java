package com.example.pages;

public abstract class Page {
	
	protected String url;
	
	public Page(String urlOfPage) {
		url = urlOfPage;
	}
	
	public String getUrl() {
		return url;
	}
}
